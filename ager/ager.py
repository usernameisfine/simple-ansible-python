#!/usr/bin/env python
import argparse
from subprocess import Popen, PIPE, TimeoutExpired
from shlex import quote
from time import sleep

# Default timeout for subprocess operations
DEFAULT_TIMEOUT = 30


def start_container(name, age):
    """Takes in name and age for 'ager' container, returns ID for running 'ager' container in background"""
    cmd = ["docker", "run", "-d", "ager", name, age]
    process = None
    raw = None
    cid = None
    try:
        process = Popen(cmd, stdout=PIPE, stderr=PIPE)
        raw = process.communicate(timeout=DEFAULT_TIMEOUT)
    except TimeoutExpired:
        print("Timed out waiting for docker run command!")
        process.terminate()
        exit(1)
    finally:
        process.terminate()
    cid = raw[0].decode("utf-8").strip('\n')
    if cid is "":
        print("Could not run 'ager'; got this back:")
        print_subprocess_output(raw)
        exit(1)
    print("docker run -d ager {} {}".format(name, age), flush=True)
    return cid


def stop_container(cid):
    """Takes in container ID to stop and stops it"""
    cmd = ["docker", "stop", cid]
    process = None
    try:
        process = Popen(cmd, stdout=PIPE, stderr=PIPE)
        process.communicate(timeout=DEFAULT_TIMEOUT)
    except TimeoutExpired:
        print("Timed out waiting for docker stop command!")
        process.terminate()
        exit(1)
    finally:
        process.terminate()


def get_container_logs(cid):
    """Takes in container ID and returns logs for the given container"""
    cmd = ["docker", "logs", cid]
    process = None
    o = None
    try:
        process = Popen(cmd, stdout=PIPE, stderr=PIPE)
        o = process.communicate(timeout=DEFAULT_TIMEOUT)
    except TimeoutExpired:
        print("Timed out waiting for docker logs command!")
        process.terminate()
        exit(1)
    finally:
        process.terminate()
    return o


def print_subprocess_output(o):
    """Takes in an output from `subprocess.communicate()` and prints it"""
    for s in o:
        if s is not None:
            print(s.decode("utf-8"), flush=True)


# Def args and parse
parser = argparse.ArgumentParser(description="Spin up an 'ager' container with provided args and stop afterwards")
parser.add_argument("-n", "--name", help="name to print")
parser.add_argument("-a", "--age", help="age for container", type=int)
args = parser.parse_args()

# Run container, wait for given 'age', get output, print, stop
container = start_container(quote(args.name), str(args.age))
sleep(args.age)
output = get_container_logs(container)
print_subprocess_output(output)
stop_container(container)
