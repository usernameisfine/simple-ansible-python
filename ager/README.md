# Ager Demo #
A simple Python script that starts a Docker container and kills it after a given amount of time.

## Prerequisites ##
* Docker installed locally
* Python 3.6+ installed locally (may work with lower versions; currently untested)

## Up and Running ##
1. Build "ager" Docker container:
    * `docker build -t 'ager' .`
2. Run `ager.py` script
    * `./ager.py -n Justin -a 3`
    * `python -n ager.py -a Justin 3`

## Test Plan ##
* Try the happy path: `python ager.py -n Justin -a 3`
    * Should return `Hi Justin, age: 3`
* Try a name with spaces: `python ager.py -n "Justin Flowers" -a 3`
    * Should return `Hi 'Justin Flowers', age: 3`
* Try injecting something dangerous in the name: `python ager.py -n "Justin\$1Flowers" -a 3`
    * Should return `Hi 'Justin$1Flowers', age: 3`
* Try shorter ages: `python ager.py -n Justin -a 1`
    * May return blank output if the Docker container doesn't have time to spin up and give output before being killed
* Try longer ages: `python ager.py -n Justin -a 10`
    * Confirm timing matches age provided
* Try providing a non int to the age arg: `python ager.py -n Justin -a 1z`
    * Should return `ager.py: error: argument -a/--age: invalid int value: '1z'`
* Try running without the "ager" image available
    * Should not crash but return output from "docker run" command like `Unable to find image 'ager:latest' locally`
* Confirm containers are actually getting killed with `docker ps`
