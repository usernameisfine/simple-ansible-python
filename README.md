# Simple Ansible and Python Demo #

This repo contains:

* An example Python program for controlling Docker containers with `subprocess` under the `ager` dir
* An example Ansible playbook for installing and configuring Docker under the `ansible` dir

There are further READMEs explaining usage in the futher levels down.