# Ansible Demo #
This playbook configures and installs Docker using the `daemon.j2` template, the `data_root` var, and the `docker_version` var. It assumes its deploying to CentOS like boxes.

## Prereqs ##
* Clean VMs with SSH keys configured for access by Ansible
* Ansible and associated executables (like `ansible-playbook`) installed and configured correctly on control machine

## Up and Running ##
1. Ensure `workers` section in `hosts` file is configured properly and Ansible can connect via SSH
2. Change default vars in `install-docker.yml` as required
3. Run `ansible-playbook -i hosts ./install-docker.yml`